var express = require('express');
var compression = require('compression');
var bodyParser = require('body-parser');
var passport = require('passport');
var http = require('http').createServer(app);
var MailListener = require("mail-listener2");
var app = express();

var firebase = require("firebase/app");
require("firebase/auth");
require("firebase/database");
var firebaseConfig = {
    apiKey: "AIzaSyDFIv9FrRBrVK33SzyWXuvwKnExJjpzuz8",
    authDomain: "emailparser-dab0f.firebaseapp.com",
    databaseURL: "https://emailparser-dab0f.firebaseio.com",
    projectId: "emailparser-dab0f",
    storageBucket: "emailparser-dab0f.appspot.com",
    messagingSenderId: "122216304679",
    appId: "1:122216304679:web:4dea8b66230052c4bebe02",
    measurementId: "G-WE3LH0XM0E"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

var database = firebase.database();
var ref = database.ref('received_emails');

app.use(compression())

app.use(bodyParser.json({ limit: '50mb', extended: true }));
app.use(bodyParser.urlencoded({ // to support URL-encoded bodies
    limit: '50mb',
    extended: true
}));

app.use(passport.initialize());
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept,Authorization, Access-Control-Allow-Headers");
    if (req.method === 'OPTIONS') {
        res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
        res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH');
        return res.status(200).json({});
    };
    next();
});



app.use(express.static(__dirname + '/'));

var mailListener = new MailListener({
    username: "enter your your email Id here",
    password: "Enter your password here",
    host: "imap.gmail.com",
    tls: true,
    port: 993,
    connTimeout: 10000, // Default by node-imap
    authTimeout: 5000, // Default by node-imap,
    //debug: console.log, // Or your custom function with only one incoming argument. Default: null
    tlsOptions: { rejectUnauthorized: false },
    mailbox: "INBOX", // mailbox to monitor
    searchFilter: ['UNSEEN', ['SINCE', 'May 18, 2020']], // the search filter being used after an IDLE notification has been retrieved
    markSeen: true, // all fetched email willbe marked as seen and not fetched next time
    fetchUnreadOnStart: true, // use it only if you want to get all unread email on lib start. Default is `false`,
    mailParserOptions: { streamAttachments: true }, // options to be passed to mailParser lib.
    attachments: true, // download attachments as they are encountered to the project directory
    attachmentOptions: { directory: "attachments/" } // specify a download directory for attachments
});


//mailListener.start(); // start listening

// stop listening
//mailListener.stop();

mailListener.on("server:connected", function () {
    console.log("imapConnected");
});

mailListener.on("server:disconnected", function () {
    console.log("imapDisconnected");
});

mailListener.on("error", function (err) {
    console.log(err);
});

mailListener.on("mail", function (mail, seqno, attributes) {
    // do something with mail object including attachments
    // console.log("emailParsed", mail.attachment);
    var mailSubject = mail.subject;
    var mailText = mail.text;
    var mailhtml = mail.html;
    var mailfrom = mail.headers.from;
    var maildate = mail.headers.date;
    var data = {
        body: mailText,
        html: mailhtml,
        subject: mailSubject,
        from: mailfrom,
        received_at: maildate
    }
    ref.push(data);
});

// mailListener.on("attachment", function (attachment) {
//     // do something with mail object including attachments
//     // console.log("emailattachment", attachment);
//     // mail processing code goes here
// });

mailListener.on("attachment", function (attachment) {
    // console.log("attachment", attachment);
});


http.listen(7000, function () {
    console.log('listening on *:' + 7000);
});

setTimeout(function () {
    mailListener.start();
}, 1000)
module.exports = app;